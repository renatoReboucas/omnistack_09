const mongoose = require('mongoose')

/**
 * type = referencia do id do ususario no mongo
 * ref = schema que sera ultilizado
user: {
  type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
}
**/

const BookingSchema = new mongoose.Schema({
  date: String,
  approved: Boolean,
  spot: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Spot'
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
})

module.exports = mongoose.model('Booking', BookingSchema)