const mongoose = require('mongoose')

/**
 * type = referencia do id do ususario no mongo
 * ref = schema que sera ultilizado
user: {
  type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
}
**/

const SpotSchema = new mongoose.Schema({
  thumbnail: String,
  company: String,
  price: Number,
  techs: [String],
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
})

module.exports = mongoose.model('Spot', SpotSchema)