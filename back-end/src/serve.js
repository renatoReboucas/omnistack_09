const express  = require('express')
const routes = require('./routes')
const mongoose = require('mongoose')

const app = express()

mongoose.connect('mongodb://user:mudar123@localhost:8081/nodeapi',{
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

// req.query acessa query params (para filtros)
// req.params acessa query (para edicao e criacao)
// req.body acessar corpo da requisicao(para criacao, edicao)

app.use(express.json())
app.use(routes)

app.listen(3333)

